#ifndef GRADECALCULATOR_H
#define GRADECALCULATOR_H

#include <QMainWindow>

namespace Ui {
class GradeCalculator;
}

class GradeCalculator : public QMainWindow
{
    Q_OBJECT

public:
    explicit GradeCalculator(QWidget *parent = 0);
    ~GradeCalculator();

private slots:
    void on_horizontalSlider_valueChanged(int value);

    void on_spinBox_valueChanged(int arg1);

    void on_goButton_clicked();

    void on_horizontalSlider_2_valueChanged(int value);

    void on_horizontalSlider_3_valueChanged(int value);

    void on_horizontalSlider_4_valueChanged(int value);

    void on_horizontalSlider_5_valueChanged(int value);

    void on_horizontalSlider_6_valueChanged(int value);

    void on_horizontalSlider_7_valueChanged(int value);

    void on_horizontalSlider_8_valueChanged(int value);

    void on_spinBox_2_valueChanged(int arg1);

    void on_spinBox_3_valueChanged(int arg1);

    void on_spinBox_4_valueChanged(int arg1);

    void on_spinBox_5_valueChanged(int arg1);

    void on_spinBox_6_valueChanged(int arg1);

    void on_spinBox_7_valueChanged(int arg1);

    void on_spinBox_8_valueChanged(int arg1);

    void on_midtermslider1_valueChanged(int value);

    void on_midtermspinbox1_valueChanged(int arg1);

    void on_midtermslider2_valueChanged(int value);

    void on_midtermspinbox2_valueChanged(int arg1);

    void on_finalslider_valueChanged(int value);

    void on_finalspinbox_valueChanged(int arg1);

    void on_cslider1_valueChanged(int value);

    void on_cspinbox1_valueChanged(int arg1);

    void on_cslider2_valueChanged(int value);

    void on_cspinbox2_valueChanged(int arg1);

    void on_cslider3_valueChanged(int value);

    void on_cspinbox3_valueChanged(int arg1);

    void on_cmidtermslider_valueChanged(int value);

    void on_cmidtermspinbox_valueChanged(int arg1);

    void on_cfinalpslider_valueChanged(int value);

    void on_cfinalpspinbox_valueChanged(int arg1);

    void on_cfinaleslider_valueChanged(int value);

    void on_cfinalespinbox_valueChanged(int arg1);

    void on_cgoButton_clicked();

private:
    Ui::GradeCalculator *ui;
};

#endif // GRADECALCULATOR_H
