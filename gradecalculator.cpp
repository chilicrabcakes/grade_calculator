#include "gradecalculator.h"
#include "ui_gradecalculator.h"

GradeCalculator::GradeCalculator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GradeCalculator)
{
    ui->setupUi(this);
}

GradeCalculator::~GradeCalculator()
{
    delete ui;
}

void GradeCalculator::on_horizontalSlider_valueChanged(int value)
{
    ui->spinBox->setValue(value);
}

void GradeCalculator::on_spinBox_valueChanged(int arg1)
{
    ui->horizontalSlider->setValue(arg1);
}
void GradeCalculator::on_horizontalSlider_2_valueChanged(int value)
{
    ui->spinBox_2->setValue(value);
}

void GradeCalculator::on_spinBox_2_valueChanged(int arg1)
{
    ui->horizontalSlider_2->setValue(arg1);
}

void GradeCalculator::on_horizontalSlider_3_valueChanged(int value)
{
    ui->spinBox_3->setValue(value);
}

void GradeCalculator::on_spinBox_3_valueChanged(int arg1)
{
    ui->horizontalSlider_3->setValue(arg1);
}

void GradeCalculator::on_horizontalSlider_4_valueChanged(int value)
{
    ui->spinBox_4->setValue(value);
}

void GradeCalculator::on_spinBox_4_valueChanged(int arg1)
{
    ui->horizontalSlider_4->setValue(arg1);
}

void GradeCalculator::on_horizontalSlider_5_valueChanged(int value)
{
    ui->spinBox_5->setValue(value);
}

void GradeCalculator::on_spinBox_5_valueChanged(int arg1)
{
    ui->horizontalSlider_5->setValue(arg1);
}

void GradeCalculator::on_horizontalSlider_6_valueChanged(int value)
{
    ui->spinBox_6->setValue(value);
}

void GradeCalculator::on_spinBox_6_valueChanged(int arg1)
{
    ui->horizontalSlider_6->setValue(arg1);
}

void GradeCalculator::on_horizontalSlider_7_valueChanged(int value)
{
    ui->spinBox_7->setValue(value);
}

void GradeCalculator::on_spinBox_7_valueChanged(int arg1)
{
    ui->horizontalSlider_7->setValue(arg1);
}

void GradeCalculator::on_horizontalSlider_8_valueChanged(int value)
{
    ui->spinBox_8->setValue(value);
}

void GradeCalculator::on_spinBox_8_valueChanged(int arg1)
{
    ui->horizontalSlider_8->setValue(arg1);
}

void GradeCalculator::on_midtermslider1_valueChanged(int value)
{
    ui->midtermspinbox1->setValue(value);
}

void GradeCalculator::on_midtermspinbox1_valueChanged(int arg1)
{
    ui->midtermslider1->setValue(arg1);
}

void GradeCalculator::on_midtermslider2_valueChanged(int value)
{
    ui->midtermspinbox2->setValue(value);
}


void GradeCalculator::on_midtermspinbox2_valueChanged(int arg1)
{
    ui->midtermslider2->setValue(arg1);
}

void GradeCalculator::on_goButton_clicked()
{
    //Calculates average value of all the sliders
   int hwSum = ui->spinBox->value() + ui->spinBox_2->value() + ui->spinBox_3->value()
           + ui->spinBox_4->value() + ui->spinBox_5->value() + ui->spinBox_6->value()
           + ui->spinBox_7->value() + ui->spinBox_8->value();

   double avgHwSum = hwSum/8;

   // Method 1: Take scores of both midterms
   int mid1 = ui->midtermslider1->value();
   int mid2 = ui->midtermslider2->value();
   int fin  = ui->finalslider->value();

   double m1_score = 0.25*avgHwSum + 0.2*mid1 + 0.2*mid2 + 0.35*fin;

   // Method 2: Take score of highest midterm
   int mid = mid1 > mid2 ? mid1 : mid2;

   double m2_score = 0.25*avgHwSum + 0.3*mid + 0.44*fin;

   int score = m1_score > m2_score ? m1_score : m2_score;

   ui->spinBox1->setValue(score);
}

void GradeCalculator::on_finalslider_valueChanged(int value)
{
    ui->finalspinbox->setValue(value);
}


void GradeCalculator::on_finalspinbox_valueChanged(int arg1)
{
    ui->finalslider->setValue(arg1);
}

void GradeCalculator::on_cslider1_valueChanged(int value)
{
    ui->cspinbox1->setValue(value);
}

void GradeCalculator::on_cspinbox1_valueChanged(int arg1)
{
    ui->cslider1->setValue(arg1);
}

void GradeCalculator::on_cslider2_valueChanged(int value)
{
    ui->cspinbox2->setValue(value);
}

void GradeCalculator::on_cspinbox2_valueChanged(int arg1)
{
    ui->cslider2->setValue(arg1);
}

void GradeCalculator::on_cslider3_valueChanged(int value)
{
    ui->cspinbox3->setValue(value);
}


void GradeCalculator::on_cspinbox3_valueChanged(int arg1)
{
    ui->cslider3->setValue(arg1);
}

void GradeCalculator::on_cmidtermslider_valueChanged(int value)
{
    ui->cmidtermspinbox->setValue(value);
}


void GradeCalculator::on_cmidtermspinbox_valueChanged(int arg1)
{
    ui->cmidtermslider->setValue(arg1);
}

void GradeCalculator::on_cfinalpslider_valueChanged(int value)
{
    ui->cfinalpspinbox->setValue(value);
}


void GradeCalculator::on_cfinalpspinbox_valueChanged(int arg1)
{
    ui->cfinalpslider->setValue(arg1);
}

void GradeCalculator::on_cfinaleslider_valueChanged(int value)
{
    ui->cfinalespinbox->setValue(value);
}

void GradeCalculator::on_cfinalespinbox_valueChanged(int arg1)
{
    ui->cfinaleslider->setValue(arg1);
}

void GradeCalculator::on_cgoButton_clicked()
{
    // Finds grade for PIC10C class
    int asg = ui->cslider1->value() + ui->cslider2->value() + ui->cslider3->value();
    double avgAsg = asg/3;

    // Method 1: Both midterm and final
    int mid = ui->cmidtermspinbox->value();
    int fin = ui->cfinaleslider->value();
    int finp = ui->cfinalpspinbox->value();

    double score1 = 0.15*avgAsg + 0.25*mid + 0.3*fin + 0.35*finp;
    double score2 = 0.15*avgAsg + 0.5*fin + 0.35*finp;

    int score = score1 > score2 ? score1 : score2;
    ui->covrspinbox->setValue(score);
}
